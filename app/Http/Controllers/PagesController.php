<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Lista;

class PagesController extends Controller
{
    public function index()
    {
        $listas = Lista::all();
        //$listas = DB::select('select * from chart');

        return view('welcome', compact('listas'));
   }

   public function add(Request $request)
   {
       $item = [
       'name' => $request->input('name'),
       'id' => $request->input('id')
       ];
    if($item['id']!=NULL)
    {
        $edit = Lista::find($item['id']);
        $edit->name = $item['name'];
        $edit->save();
        return view('edit', compact('item'));
    }
    if($item['name']!=NULL) {
        $list = new Lista;
        $list->name = $request->name;
        $list->save();
        //$list = DB::insert('insert into chart (name) values (?)', [$item['name']]);
        return view('add', compact('item'));
       }
   }

   public function delete(Request $request)
   {
       $item = [
           'id' => $request->input('id')
       ];
       if($item['id']!=NULL)
       {
           $todel = Lista::find($item['id']);
           if ($todel!=NULL)
           {
                $todel->delete();
           }
       }
   }
   
   public function edit(Request $request)
   {
    $item = [
        'id' => $request->input('id')
    ];
    if($item['id']!=NULL)
    {
        
        return view('edit', compact('item'));
    }
    return redirect('/');
   }

   public function show()
   {
    $listas = Lista::all();
       return view('show', compact('listas'));
   }

}
