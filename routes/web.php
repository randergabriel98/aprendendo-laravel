<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'PagesController@show')->name('home');
Route::get('/index', 'PagesController@index');
Route::put('/add', 'PagesController@add');
Route::get('/add', 'PagesController@add');
Route::delete('/delete', 'PagesController@delete');
Route::get('/edit', 'PagesController@edit');