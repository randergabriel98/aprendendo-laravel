        <div id="index_content" class="container">
            <h2>Lista:</h2>
        @if (isset($listas))
            @foreach ($listas as $lista)
            <div id="id_{{ $lista->id }}" class="container">
                <h4>{{ $lista->name }}
                <form class="delete" action="delete" method="post">
                    {{ csrf_field() }}
                    <button><input type="hidden" name="id" value="{{$lista->id}}"><input type="hidden" name="_method" value="delete" />Apagar</button>
                </form>
                <button onclick="getEditForm({{$lista->id}})" id="fl_{{$lista->id}}" class="add">Editar</button>
                <form class="add edit" style="display: none;" id="fe_{{ $lista->id }}" action="add" method="post">
                    <h3>Novo nome:</h3>
                    <input type="hidden" name="id" value="{{$lista->id}}">
                    <input type="hidden" name="_method" value="PUT"/>
                    Nome: <input type="text" name="name">
                    <button name="id" type="submit" value="{{$lista->id}}">Editar</button>
                    {{ csrf_field() }}
                </form>
            </div>
            @endforeach
        @endif
        </div>